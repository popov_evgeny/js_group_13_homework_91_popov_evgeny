const express = require('express');
const cors = require('cors');
const { nanoid } = require('nanoid');
const app = express();
require('express-ws')(app);

const port = 8000;


app.use(cors());


const activeConnections = {};
const arrayPixelCoordinates = [];


app.ws('/canvas', function (ws, req) {
  const id = nanoid();
  activeConnections[id] = ws;

  ws.send(JSON.stringify({
    type: 'PREV_CIRCLE',
    coordinates: arrayPixelCoordinates,
  }));

  ws.on('message', (msg) => {
    const decodedMessage = JSON.parse(msg);
    switch (decodedMessage.type) {
      case 'SEND_CIRCLE':
        Object.keys(activeConnections).forEach(id => {
          const conn = activeConnections[id];
          arrayPixelCoordinates.push(decodedMessage.coordinates);
          conn.send(JSON.stringify({
            type: 'NEW_CIRCLE',
            pixelCoordinates: decodedMessage.coordinates,
            color: decodedMessage.color,
            size: decodedMessage.size
          }));
        });
        break;
      default:
        console.log('Unknown type!')
    }
  });

  ws.on('close', () => {
    console.log('Client disconnected!');
    delete activeConnections[id];
  });
});


app.listen(port, () => {

  console.log(`Server started on ${port} port!`);

});