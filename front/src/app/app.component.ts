import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';


interface Pixel {
  x: number,
  y: number,
  color: string,
  size: number
}

interface ServerMessage {
  type: string,
  coordinates: Pixel [];
  pixelCoordinates: Pixel
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})

export class AppComponent implements  AfterViewInit, OnDestroy {
  ws!: WebSocket;
  @ViewChild('canvas') canvas!: ElementRef;
  circleColor = 'black';
  circleSize = 5;

  ngAfterViewInit() {
    this.ws = new WebSocket('ws://localhost:8000/canvas');
    this.ws.onclose = () => console.log('ws closed')

    this.ws.onmessage = event => {
      const decodedMessage: ServerMessage = JSON.parse(event.data);

      if (decodedMessage.type === 'PREV_CIRCLE') {
        decodedMessage.coordinates.forEach( cord => {
          this.drawCircle(cord.x, cord.y, cord.color, cord.size);
        })
      }

      if (decodedMessage.type === 'NEW_CIRCLE') {
        const {x, y, color, size} = decodedMessage.pixelCoordinates;
        this.drawCircle(x, y, color, size);
      }
    };

  }

  drawCircle(x: number, y: number, color: string, size: number) {
    const canvas: HTMLCanvasElement = this.canvas.nativeElement;
    const ctx = canvas.getContext("2d")!;
    ctx.fillStyle = color;
    ctx.beginPath();
    ctx.arc( x, y, size,0,2*Math.PI);
    ctx.fill();
  }

  getColor(event: Event) {
    this.circleColor = (<HTMLSelectElement> event.target).value;
  }

  getSize(event: Event) {
    this.circleSize = parseInt((<HTMLSelectElement> event.target).value);
  }

  onCanvasClick(event: MouseEvent) {
    const x = event.offsetX;
    const y = event.offsetY;
    this.ws.send(JSON.stringify({
      type: 'SEND_CIRCLE',
      coordinates: {x, y, color: this.circleColor, size: this.circleSize},
    }))
  }


  ngOnDestroy() {
    this.ws.close();
  }
}
